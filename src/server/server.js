var express = require('express')
	, path = require('path')
	, bodyParser = require('body-parser')
	, methodOverride = require('method-override')
	, app = express()
	, mongoose = require('mongoose')
	, db = mongoose.connection
;

var clientDir = path.join(__dirname, '../client');
console.log(clientDir);
app.use(express.static(clientDir));
app.use(bodyParser());
app.use(methodOverride());

app.get('/*', function(req,res, next){
	res.format({
		html: function(){
			res.sendfile(path.join(clientDir, 'index.html'));
		},
		json: function(){ next(); }
	})
})

//apis
require('./routes/user').serverInit(app);



mongoose.connect('mongodb://localhost/mean');

db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function callback () {
	

	var server = app.listen(3000, function() {
		console.log('Listening on port %d', server.address().port);
	});
});

