var auth = require('../auth')
	, requireAuth=auth.requireAuth
	, userModel = require('../models/user')
;


module.exports.serverInit=function(app){

	app.all('/user*', requireAuth);

	app.get('/user/:id', getUser);
	app.post('/user', createUser);
	app.put('/user/:id', updateUser);	
	app.del('/user/:id', deleteUser);
}


var getUser = function(req, res){
	userModel.findById(req.params.id, function (err, user) {
		if(err) return res.send(err);
		
		res.send(user);
	});
	
}

var createUser = function(req, res){
	userModel.create(req.body, function(err, user){
		if(err) return res.send(err);
		
		res.send(user);
	});	
}

var updateUser = function(req, res){
	res.send(200);
}

var deleteUser = function(req, res){
	res.send(200);
}