angular.module('user', ['ui.router'])
	.config(function($stateProvider, $urlRouterProvider, $locationProvider) {
		//$locationProvider.html5Mode(true);
		
		$stateProvider			
			.state('user', {
				url: "/user/",
				templateUrl: "user/user.html",
				controller:'userCtrl'
			})
			.state('user.detail', {
				url: "/user/:id",
				templateUrl: "user/user.detail.html",
				controller:function($scope, $stateParams){
				
				}
			})
		
	});;